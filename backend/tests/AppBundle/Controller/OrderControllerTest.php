<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/orders', [
            'from'=>'01/01/1999'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'The "Content-Type" header is "application/json"'
        );

        $content = json_decode($client->getResponse()->getContent());

        $this->assertEquals(false,  $content->error);
        $this->assertTrue(
            !empty($content->items) and is_array($content->items)
            ,'Empty items'
        );

        if(!empty($content->items) and is_array($content->items)) {
            $firstItem = $content->items[0];
            $this->assertTrue(isset($firstItem->name), 'Check Name');
            $this->assertTrue(isset($firstItem->amount), 'Check Amount');
            $this->assertTrue(isset($firstItem->gross_sales), 'Check GrossSales');
            $this->assertTrue(isset($firstItem->tax), 'Check Tax');
            $this->assertTrue(isset($firstItem->net_sales), 'Check NetSales');
        }


        // Указываем неверный формат даты
        $client->request('GET', '/orders', [
            'from'=>'abcde',
            'to'=>'abcde'
        ]);
        $content = json_decode($client->getResponse()->getContent());
        $this->assertTrue(
            (!empty($content->error_message) and $content->error_message == 'Invalid dates')
            ,'Date validation'
        );
    }
}
