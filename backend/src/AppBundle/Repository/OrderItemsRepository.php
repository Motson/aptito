<?php

namespace AppBundle\Repository;
/**
 * OrderItemsRepository
 *
 */
class OrderItemsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $orderFrom
     * @param $orderTo
     * @return array
     */
    public function getOrderItemsByDates($orderFrom, $orderTo) {
        $query = $this->createQueryBuilder('oi');
        $query->select('op.name')
            ->addSelect('oi.qty as amount')
            ->addSelect('(oi.qty*oi.price+oi.tax) as gross_sales')
            ->addSelect('oi.tax')
            ->addSelect('(oi.qty*oi.price) as net_sales')
            ->where('oi.date > :from')->setParameter(':from', $orderFrom)
            ->andWhere('oi.date < :to')->setParameter(':to', $orderTo)
            ->join('oi.order', 'o')
            ->join('oi.plate', 'op');
        return $query->getQuery()->getArrayResult();
    }
}
