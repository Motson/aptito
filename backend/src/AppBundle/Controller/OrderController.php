<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * @Route("/orders", name="orders")
     */
    public function indexAction(Request $request)
    {
        $error = false;
        $errorMessage = '';
        $results = [];

        $from = $request->get('from');
        $to = $request->get('to');

        $from = empty($from) ? mktime(0,0,0) : strtotime($from);
        $to = empty($to) ? mktime(23,59,59) : strtotime($to);
        if(!empty($from) and !empty($to)) {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:OrderItems');
                $results = $repository->getOrderItemsByDates($from, $to);
                foreach ($results as &$resultsItem) {
                    $resultsItem['gross_sales'] = (float)$resultsItem['gross_sales'];
                    $resultsItem['net_sales'] = (float)$resultsItem['net_sales'];
                }
            } catch (Exception $e) {
                $errorMessage = 'Undefined Error';
                $error = true;
            }
        } else {
            $error = true;
            $errorMessage = 'Invalid dates';
        }

        $response = new JsonResponse([
            'error'=>$error,
            'error_message'=>$errorMessage,
            'items'=>$results,
        ], 200, ['Access-Control-Allow-Origin'=> '*']);
        return $response;
    }
}
