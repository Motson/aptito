# README #

Репозиторий состоит из двух приложений:

1. Приложение вывода таблицы заказов с фильтрацией по датам, сортировкой по столбцам. Написан на *AngularJS*. Расположено в папке *www*

2. php сервис, который обрабатывает запросы и выводит данные в формате json. Написан на *Symfony 3*. Расположен в папке *backend*

### Установка и конфигурация php-сервиса ###

* Загрузка зависимостей из папки *backend*

```
#!console

composer update
```

* Конфигурация БД в файле app/config/parameters.yml

```
#!php
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: aptito_current
    database_user: root
    database_password: null

```

* Запуск сервиса (будет доступен по адресу http://127.0.0.1:8000)

```
#!console

php bin/console server:run
```

* Запуск тестов

```
#!console

phpunit
```

### Установка и конфигурация фроненд-приложения ###

* Загрузка зависимостей через npm

```
#!console

npm update
```