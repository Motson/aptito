var App = angular.module('aptitoApp', ['ngResource', '720kb.datepicker']);

App.config(['$resourceProvider', function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
}]);

App.factory('Order', [
    '$resource', function($resource) {
        var apiUrl = 'http://127.0.0.1:8000';
        return $resource(apiUrl+'/orders', {}, {
            read: {method: 'GET', params: {from: '@from', to: '@to'}, isArray: false}
        });
    }
]);

App.controller('ordersCtrl', function($scope, Order) {
    $scope.fromDate = null;
    $scope.toDate = null;
    $scope.sortType     = 'name';
    $scope.sortReverse  = false;
    $scope.orderItems = [];
    $scope.updating = false;

    $scope.getColumnTotal = function(fieldName){
        var total = 0;
        for(var i = 0; i < $scope.orderItems.length; i++) {
            total += $scope.orderItems[i][fieldName];
        }
        return total;
    };

    $scope.dateValidation = function (value) {
        var liveDate;
        try {
            liveDate = new Date(value);
        } catch(e) {}
        if (value && (!liveDate || !isFinite(liveDate))) {
            $scope.error = "Invalid dates";
        } else {
            $scope.error = false;
            $scope.loadData();
        }
    };

    $scope.$watch('fromDate', function(value) {
        $scope.dateValidation(value);
    });
    $scope.$watch('toDate', function(value) {
        $scope.dateValidation(value);
    });

    $scope.loadData = function () {
        if($scope.updating === false) {
            $scope.updating = true;
            Order.read({from: $scope.fromDate, to: $scope.toDate}, function(data) {
                $scope.orderItems = data.items;
                if(data.error) {
                    $scope.error = data.error_message;
                }
                $scope.updating = false;
            });
        }
    };

    $scope.sort = function(fieldName) {
        $scope.sortType = fieldName;
        $scope.sortReverse = !$scope.sortReverse;
    };
});